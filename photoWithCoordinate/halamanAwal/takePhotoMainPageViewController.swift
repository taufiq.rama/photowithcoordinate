//
//  ViewController.swift
//  photoWithCoordinate
//
//  Created by Taufiq Ramadhany on 21/04/20.
//  Copyright © 2020 Taufiq Ramadhany. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class takePhotoMainPageViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CLLocationManagerDelegate {

    @IBOutlet weak var displayedImageMainScreen: UIImageView!
    @IBOutlet weak var locationCoordinateLabel: UILabel!
    @IBOutlet weak var takePhotoButtonName: UIButton!
    
    
    let locationManager = CLLocationManager()
    var locationCoordinateLatitude:String = ""
    var locationCoordinateLongitude:String = ""
    var nameOfLocation = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //use for application open and running in backgroun
        //locationManager.requestAlwaysAuthorization()
        
        //use when application in use
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
            
        }
        
    }
        
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            //print(location.coordinate)
            locationCoordinateLatitude = String(location.coordinate.latitude)
            locationCoordinateLongitude = String(location.coordinate.longitude)
            print(CLGeocodeCompletionHandler.self)
            lookUpCurrentLocation { geoLoc in
                //print(geoLoc?.locality ?? "unknown Geo location")
                //self.nameOfLocation = geoLoc?.locality as! String
                self.nameOfLocation = String(geoLoc?.locality ?? "unkown Geo Location")
            }
        }
    }
    
    func lookUpCurrentLocation(completionHandler: @escaping (CLPlacemark?)
        -> Void ) {
        // Use the last reported location
        if let lastLocation = self.locationManager.location {
        let geocoder = CLGeocoder()
        // Look up the location and pass it to the completion handler
            geocoder.reverseGeocodeLocation(lastLocation, completionHandler: {(placemarks, error) in
                if error == nil {
                    let firstLocation = placemarks?[0]
                    completionHandler(firstLocation)
                } else {
                    completionHandler (nil)
                }
            })
        
    }
        else {
            completionHandler (nil)
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if(status == CLAuthorizationStatus.denied){
            showLocationDisablePopup()
        }
    }
    
    func showLocationDisablePopup() {
        let alertLocationDenied = UIAlertController (title: "Mohon ijinkan aplikasi ini melihat lokasi anda", message: "Lokasi anda dibutuhkan untuk melakukan absensi secara online", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "cancel", style: .cancel, handler: nil)
        alertLocationDenied.addAction(cancelAction)
        
        let openAction = UIAlertAction(title: "Open Setting", style: .default) {(action) in
            if let url = URL(string: UIApplication.openSettingsURLString){
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        alertLocationDenied.addAction(openAction)
        
        self.present(alertLocationDenied, animated: true, completion: nil)
    }
    
    @IBAction func takePhotoButton(_ sender: AnyObject) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera){
            let takePhoto = UIImagePickerController()
            takePhoto.delegate = self
            takePhoto.sourceType = UIImagePickerController.SourceType.camera
            takePhoto.allowsEditing = false
            self.present(takePhoto, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let takenImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            displayedImageMainScreen.contentMode = .scaleToFill
            displayedImageMainScreen.image = takenImage
            //locationCoordinateLabel.text = locationCoordinateLongitude + "," + locationCoordinateLatitude
            locationCoordinateLabel.text = "You are in " + nameOfLocation
            takePhotoButtonName.isHidden = true
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
}
